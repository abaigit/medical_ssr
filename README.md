### Annotation
Project <b><strong>`Medical`</strong></b> aims to provide online services like: 
online appointment to doctors, storing all patient's medical data (tests, visits to doctors, 
doctors' recommendations) in one structured platform, which is useful, because this platform
can eliminate the need to store all data in paper form and in a scattered state. Users can 
access their medical history any time from anywhere.

This project can be run as standard django project `python manage.py runserver`, or
can be used in Docker. Project has `docker-compose.yml` file which describes main
services to be build of. To start you need to have installed <b>Docker & docker-compose</b>
on your computer. If your system meets the requirements you can type and run command
`docker-compose up --build` or `docker-compose up --build --detach` to run it in 
detach mode.

### Celery
This project uses celery to send confirmation emails in background, and to generate <b>pdf</b>
file of <code>Patient's Card</code>.
When using standard `./manage.py runserver` command, open on more terminal and run celery
with command `celery -A server worker -l info`, and to use beat you can type 
`celery -A server worker --beat -l info`.<br/>
<u>Note:</u> This project creates doctor's appointments if there is at least one doctor and
one patient. Scheduled to 8:00 AM every day.

### Environment 

Create virtual environment and after activating it, install Requirements `pip install -r requirements.txt`<br/>
You need to provide environment variables. I would suggest to create directory `conf` and place 
there file named `vars.env` (if this path exists envparse will read it). Also, to use <b>Docker-compose</b> you 
gonna need `dockers.env` file inside `conf` directory.<br/>
Variables that needs to be declared are:

    SECRET_KEY
    DEBUG
    ALLOWED_HOSTS
    CORS_ORIGINS
    DATABASE_URL
    EMAIL_BACKEND
    EMAIL_HOST
    EMAIL_PORT
    EMAIL_USE_TLS
    EMAIL_HOST_USER
    EMAIL_HOST_PASSWORD
    CELERY_BROKER_URL
    CELERY_RESULT_BACKEND
    LOCAL_DEV

### Learn More

You can learn more in the [Django documentation](https://docs.djangoproject.com/en/3.1/).<br/>
To learn DRF, check out the [Django Rest Framework documentation](https://www.django-rest-framework.org/).<br/>
JSON Web Token Authentication support for Django REST Framework [REST framework JWT Auth](https://jpadilla.github.io/django-rest-framework-jwt/).<br/>
First steps using Django with [Celery](https://docs.celeryq.dev/en/stable/django/index.html)<br/> 
Deploy Django application on [Heroku Devcenter](https://devcenter.heroku.com/categories/python-support)<br/> 
For hosting static and media files use [Google Cloud Storage](https://cloud.google.com/storage/docs/hosting-static-website)<br/>

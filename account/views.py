from django.shortcuts import render, redirect, HttpResponse, Http404
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth import (
    authenticate, login as site_login,
    logout as site_logout, get_user_model
)
from django.contrib.auth.tokens import default_token_generator
from django.contrib.auth.forms import PasswordChangeForm, PasswordResetForm
from django.contrib.sites.shortcuts import get_current_site
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.utils.encoding import force_bytes, force_text
from .forms import CreateUserForm, UserDetailChangeForm
from .decorators import not_authenticated
from .tasks import celery_send_mail


User = get_user_model()


@login_required(login_url='account:login')
def panel(request):
    return render(request, 'account/panel.html', {})


@not_authenticated
def signup(request):
    form = CreateUserForm()

    if request.method == 'POST':
        form = CreateUserForm(request.POST)
        if form.is_valid():
            user = form.save()
            domain = str(get_current_site(request).domain)

            celery_send_mail.apply_async(kwargs={
                "subj": 'Activate your account on Medical',
                "tmplt_name": 'account/account_active_email.html',
                "user_id": user.pk,
                "domain": domain,
            })

            messages.success(request, 'Congratulations! you\'ve been registered. Please activate in your email')
            return redirect('account:login')
    return render(request, 'account/signup.html', {"form": form})


def activate_user(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and default_token_generator.check_token(user, token):
        user.is_active = True
        user.save()
        site_login(request, user)
        messages.success(request, 'Thank you for your email confirmation. Now you\'re logged in')
        return redirect('web:home')
    else:
        messages.warning(request, 'Activation link is invalid!')
        raise Http404()
        # return redirect('account:login')


@not_authenticated
def login(request):
    if request.method == 'POST':
        cred = request.POST
        email = cred.get('email')
        pswrd = cred.get('password')
        auth = authenticate(request, username=email, password=pswrd)
        if auth is not None:
            site_login(request, auth)
            return redirect('account:panel')
    return render(request, 'account/login.html', {})


def logout(request):
    site_logout(request)
    return redirect('account:login')


@login_required(login_url='account:login')
def password_change(request):
    user = request.user
    form = PasswordChangeForm(user)
    if request.method == 'POST':
        form = PasswordChangeForm(user, request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, "Your password has changed")
            site_logout(request)
            return redirect('account:login')
    return render(request, 'account/password_change.html', {'form': form})


@not_authenticated
def password_reset(request):
    if request.method == 'POST':
        email = request.POST.get('email')
        if User.objects.filter(email=email).exists():
            user = User.objects.get(email=email)
            domain = str(get_current_site(request).domain)

            celery_send_mail.apply_async(kwargs={
                "subj": 'Password Reset on Medical Platform',
                "tmplt_name": 'account/password_reset_email.html',
                "user_id": user.pk,
                "domain": domain
            })

            messages.info(request, 'Your password is reset. Confirmation email is sent')
    return render(request, 'account/password_reset.html', {})


@not_authenticated
def password_reset_confirm(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
        token_check = default_token_generator.check_token(user, token)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if not token_check:
        messages.warning(request, "Confirmation Link is Invalid")
        raise Http404()

    if request.method == 'POST':
        pw1 = request.POST.get('password1')
        pw2 = request.POST.get('password2')
        if pw1 == pw2:
            user.set_password(pw1)
            user.save()
            messages.success(request, "CONGRATS, YOUR PASSWORD IS RESET. YOU MAY LOG IN")
            return redirect('account:login')
        else:
            messages.warning(request, "PASSWORDS DON'T MATCH")
    return render(request, 'account/password_reset_confirm.html', {})


@login_required(login_url='account:login')
def account_update(request):
    form = UserDetailChangeForm(instance=request.user)
    if request.method == 'POST':
        form = UserDetailChangeForm(request.POST, instance=request.user)
        if form.is_valid():
            form.save()
            messages.success(request, message="Your profile updated!")
    return render(request, 'account/account_update.html', {"form": form})

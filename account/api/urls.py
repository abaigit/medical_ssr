from django.urls import path
from rest_framework_jwt.views import obtain_jwt_token
from . import views

app_name = 'account-api'

urlpatterns = [
    path('token-auth/', obtain_jwt_token, name='token-auth'),

    path('signup/', views.signup_view, name='signup'),
    path('update/', views.account_update, name='account_update'),

    path('password/update/', views.password_update, name="password_update"),
]

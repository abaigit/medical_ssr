from django.contrib.auth import get_user_model
from django.contrib.sites.shortcuts import get_current_site
from django.utils.decorators import method_decorator
from rest_framework.generics import ListAPIView, CreateAPIView
from rest_framework.views import APIView, View
from rest_framework.response import Response
from rest_framework import exceptions
from rest_framework.decorators import api_view, renderer_classes, permission_classes
from rest_framework.permissions import IsAuthenticatedOrReadOnly, IsAuthenticated
from account.tasks import celery_send_mail
from .serializers import (UserBaseSerializer, UserSignupSerializer,
                          UserPasswordUpdateSerializer)

User = get_user_model()


class UserListAPIView(ListAPIView):  # temporary
    serializer_class = UserBaseSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]

    def get_queryset(self):
        return User.objects.all()#.filter(position__in=["MN", "DR"])


@api_view(http_method_names=['POST'])
def signup_view(request):
    rdata = request.data
    serialized = UserSignupSerializer(data=rdata)
    if serialized.is_valid(raise_exception=True):
        user = serialized.save()
        domain = str(get_current_site(request).domain)
        celery_send_mail.delay("API - CONFIRMo Email", 'account/signup_confirm_email.html', user.id, domain)
    return Response({"success": "confirmation email is sent"})


@api_view(http_method_names=["PATCH", "PUT"])
@permission_classes([IsAuthenticated])
def account_update(request):
    rdata = request.data
    try:
        user = User.objects.get(pk=rdata.get("id"))
    except User.DoesNotExist:
        raise exceptions.NotFound
    serialized = UserBaseSerializer(data=rdata, partial=True, instance=user)
    if serialized.is_valid(raise_exception=True):
        serialized.save()
    return Response(serialized.data)


@api_view(http_method_names=["PATCH", "PUT"])
@permission_classes([IsAuthenticated])
def password_update(request):
    rdata = request.data
    sr = UserPasswordUpdateSerializer(instance=request.user, partial=True, data=rdata)
    if sr.is_valid(raise_exception=True):
        sr.save(new_password=sr.validated_data.get("password_new1"))
    return Response(sr.data)

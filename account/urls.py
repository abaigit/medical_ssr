from django.urls import path, re_path
from . import views

app_name = 'account'

urlpatterns = [
    path('', views.panel, name='panel'),
    path('update/', views.account_update, name='update'),
    path('signup/', views.signup, name='signup'),
    path('activate/<uidb64>/<token>/', views.activate_user, name='activate'),
    path('login/', views.login, name='login'),
    path('logout/', views.logout, name='logout'),
    path('password_change/', views.password_change, name='password_change'),

    path('password_reset/', views.password_reset, name='password_reset'),
    path('password_reset/confirm/<uidb64>/<token>/', views.password_reset_confirm, name="password_reset_confirm"),

]

from django.contrib import admin
from .models import (
    PatientCard, PatientNoteEntry,
    PatientAnalysis, CalendarEvent,
    EventComment, DoctorsDossier,
    Service, Department
)


@admin.register(PatientCard)
class PatientCardAdmin(admin.ModelAdmin):
    pass


@admin.register(PatientNoteEntry)
class PatientNoteEntryAdmin(admin.ModelAdmin):
    list_display = ('patient_card', 'created', 'made_by', 'id')


@admin.register(PatientAnalysis)
class PatientAnalysisAdmin(admin.ModelAdmin):
    list_display = ('patient_card', 'created', 'id')


@admin.register(CalendarEvent)
class CalendarEventAdmin(admin.ModelAdmin):
    list_display = ('id', 'status', 'doctor', 'patient', 'date_time')


@admin.register(EventComment)
class EventCommentAdmin(admin.ModelAdmin):
    pass


@admin.register(DoctorsDossier)
class DoctorsDossierAdmin(admin.ModelAdmin):
    list_display = ("user", "get_user_name", "job_position")

    @admin.display(description='Full Name')
    def get_user_name(self, obj):
        return obj.user.get_fullname()


@admin.register(Service)
class ServiceAdmin(admin.ModelAdmin):
    search_fields = ("name",)
    list_display = ("name", "price")


@admin.register(Department)
class DepartmentAdmin(admin.ModelAdmin):
    autocomplete_fields = ('services_list',)
    readonly_fields = ('slug',)

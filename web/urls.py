from django.urls import path
from . import views

app_name = 'web'

urlpatterns = [
    # PATIENT VIEW
    path('', views.HomeView.as_view(), name='home'),
    path('patient/', views.PatientRoom.as_view(), name='patient-card'),
    path('patient/card/<int:card_id>/download/', views.DownloadView.as_view(), name='card-download'),
    path('patient/tests/<int:test_id>/', views.AnalyzDetailView.as_view(), name='patient-test'),
    path('patient/appoint/', views.PatientMakeAppoint.as_view(), name='patient-appoint'),

    # DOCTOR VIEW
    path('doctor/', views.DoctorRoomView.as_view(), name='doctor-room'),
    path('doctor/patient_card/<patient_card_id>/', views.DoctorRoomPatientCardView.as_view(), name='doctor-room-pat'),
    path('doctor/patient_card/<patient_card_id>/entry/', views.DoctorPatientEntryView.as_view(), name='doctor-pat-entry'),
    path('doctor/appointments/', views.DoctorAppointments.as_view(), name='doctor-appointments'),

    # CLIENT VIEW
    path('med/about/', views.AboutView.as_view(), name='med-about'),
    path('med/services/', views.ServicesView.as_view(), name='med-services'),
    path('med/departments/', views.DepartmentsView.as_view(), name='med-departments'),
    path('med/departments/<slug:slug>/', views.DepartmentDetailView.as_view(), name='med-department-detail'),
    path('med/doctors/', views.DoctorsListView.as_view(), name='med-doctors'),
    path('med/doctors/<int:dossier_id>/', views.DoctorsDossierDetailView.as_view(), name='med-doctor-detail'),
    path('med/appoint/', views.ClientMakeAppoint.as_view(), name='med-appoint'),
    path('med/appoint/confirmation/', views.AppointConfirmation.as_view(), name='appoint-confirm'),
    path('med/contact/', views.ContactView.as_view(), name='med-contact'),
]

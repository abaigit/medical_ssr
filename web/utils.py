from django.db.models.query_utils import Q


def patient_card_dir_path(instance, filename):
    file_type = filename.split('.')[-1]
    path = f"patients/cards/{instance.user.email}/uid_{instance.user.pk}_{instance.user.get_fullname()}.{file_type}"
    return path


def patient_card_dir_path_level(instance, filename):
    file_type = filename.split('.')[-1]
    path = f"patients/cards/{instance.patient_card.user.email}/uid_{instance.patient_card.user.pk}_{instance.patient_card.user.get_fullname()}.{file_type}"
    return path


def limit_patient_choices():
    return {"position": "PT"}


def limit_doctor_choices():
    return {"position": "DR"}


def limit_doctors_dosier_choices():
    qs = Q(position="DR") & Q(doctorsdossier__isnull=True)
    return qs


def limit_note_entry():
    qs = Q(position="DR") | Q(position='MN') # note can be written by Doctor or Manager
    return qs


def file_extensions():
    return ['txt', 'pdf', 'csv']

import datetime
import string
import random
from pathlib import Path
from django.utils.timezone import get_current_timezone
from django.conf import settings
from django.contrib.auth import get_user_model
from django.template.loader import render_to_string
from django.core.mail import send_mail
from celery import shared_task
import pdfkit
from web.models import CalendarEvent

User = get_user_model()


@shared_task(name='send_visit_reminder')
def send_visit_reminder(patient_id, doctor, date_time):
    patient = User.objects.get(id=patient_id)

    send_mail(
        "Reminder. Visit to doctor",
        "",
        settings.EMAIL_HOST_USER,
        [patient.email],
        html_message=render_to_string("web/visit_reminder.html", {
            "patient": patient,
            "doctor": doctor,
            "date_time": date_time
        })
    )
    return f"Reminder sent to {patient.email}"


@shared_task(name="process_pt_card")
def process_pt_card(data):
    entries = data["entries"]
    tests = data["tests"]
    user = data.get("user")

    card_path = f'{settings.BASE_DIR}/tmp_files'
    Path(card_path).mkdir(parents=True, exist_ok=True)
    card_file = f'{card_path}/card.pdf'

    html_layout = render_to_string("web/card_download_layout.html", {
        "user": user,
        "entries": entries,
        "tests": tests
    })
    pdfkit.from_string(html_layout, card_file)

    return card_file


@shared_task(name="event_creator")
def event_creator(*args):
    hours_list = [h for h in range(8, 20)]

    def task_executor():
        doctors = User.objects.filter(position='DR')
        patients = User.objects.filter(position='PT')
        if doctors.count() < 1 or patients.count() < 1:
            return "No doctors or patients to appoint"
        dr = random.choice(doctors)
        pt = random.choice(patients)
        today = datetime.datetime.today()
        y, m, d = today.year, today.month, today.day
        hour = random.choice(hours_list)
        hours_list.remove(hour)
        ts = datetime.datetime(y, m, d, hour, 0, 0, tzinfo=get_current_timezone())
        nt = ' '.join((random.choice(string.ascii_lowercase) for i in range(55)))
        CalendarEvent.objects.create(doctor=dr, patient=pt, date_time=ts, notes=nt)

    for j in range(12):
        task_executor()

    return f"Events for today are created"

import csv
import pandas as pd
import datetime
from django.db.models import Q
from django.contrib.auth import get_user_model
from django.shortcuts import render, get_object_or_404, redirect
from django.views.generic import View, DetailView, ListView
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.http import FileResponse, Http404
from django.template import loader
from .models import (PatientCard, PatientAnalysis,
                     PatientNoteEntry, DoctorsDossier,
                     Service, Department, CalendarEvent)
from .forms import PatientEntryForm, PatientMakeAppointForm, ClientMakeAppointForm
from .tasks import process_pt_card
from web.api.serializers import PatientCardSerializer

User = get_user_model()


# BASE CLIENT VIEWS
class HomeView(View):
    template_name = 'web/home.html'

    def get(self, request):
        return render(request, self.template_name, {})


class AboutView(View):
    template_name = 'web/about.html'

    def get(self, request):
        services = Service.objects.all()[:4]
        doctors = User.objects.filter(position='DR')
        return render(request, self.template_name, {"services": services, "doctors": doctors})


class ServicesView(View):
    template_name = 'web/services.html'

    def get(self, request):
        services = Service.objects.all()
        return render(request, self.template_name, {"services": services})


class DepartmentsView(View):
    template_name = 'web/departments.html'

    def get(self, request):
        departments = Department.objects.all()
        return render(request, self.template_name, {"departments": departments})


class DepartmentDetailView(DetailView):
    template_name = 'web/department_detail.html'
    model = Department
    context_object_name = 'department'


def handler404(request, *args, **argv):
    return render(request, '404.html', {}, status=404)


# AUTHENTICATED PATIENT VIEW
@method_decorator(login_required(login_url='account:login'), name='dispatch')  # todo: change to: LoginRequiredMixin
class PatientRoom(View):
    tempate_name = 'web/patient_card.html'

    def get(self, request, *args, **kwargs):
        context = {"card": None, "analizy": None}
        user = request.user

        if user.position != 'PT':  # todo: make via decorator
            raise Http404("Oops")

        try:
            card = PatientCard.objects.get(user=user)
        except (TypeError, PatientCard.DoesNotExist):
            card = None

        context['card'] = card
        return render(request, self.tempate_name, context)


class DownloadView(View):
    def get(self, request, card_id):
        card = PatientCard.objects.get(id=card_id)
        data = PatientCardSerializer(instance=card).data
        res = process_pt_card.delay(data)

        return FileResponse(open(res.get(), 'rb'), as_attachment=True, content_type='application/pdf')
        # return FileResponse(open(f"{settings.BASE_DIR}/media/photos/cod.jpg", 'rb'), as_attachment=True)


class AnalyzDetailView(View):  # todo: refactor 'Analyz' to 'Test'
    template_name = 'web/analyz_detail.html'

    def get(self, request, test_id):
        analyz = get_object_or_404(PatientAnalysis, pk=test_id)

        with open(analyz.csv_file.path, 'r') as file:
            reader = csv.reader(file)
            data = list(reader)

        df = pd.read_csv(analyz.csv_file.path, index_col=0)
        book = df.to_html()

        return render(request, self.template_name, {"analyz": analyz, "data": data, "book": book})


# DOCTORS VIEWS
@method_decorator(login_required(login_url='account:login'), name='dispatch')
class DoctorRoomView(View):
    template_name = 'web/doctor_room.html'

    def get(self, request):
        doctor = request.user
        today = datetime.datetime.today().date()
        appointments = doctor.schedules.filter(date_time__date=today)
        patient_list = User.objects.filter(Q(position='PT') & Q(patientcard__isnull=False)).distinct()
        # patient_list = User.objects.filter(first_name__iexact='administrator').distinct()
        return render(request, self.template_name, {"patient_list": patient_list, "appointments": appointments})


@method_decorator(login_required(login_url='account:login'), name='dispatch')
class DoctorRoomPatientCardView(DetailView):
    template_name = 'web/patient_card.html'
    model = PatientCard
    pk_url_kwarg = "patient_card_id"
    context_object_name = 'card'
    # extra_context = {"billy": User.objects.last()}  # works fine


class DoctorPatientEntryView(View):
    template_name = 'web/patient_entry_page.html'

    def get(self, request, patient_card_id):
        pt_card = PatientCard.objects.get(id=patient_card_id)
        made_by = request.user
        form = PatientEntryForm(initial={"patient_card": pt_card, "made_by": made_by})
        return render(request, self.template_name, {"form": form})

    def post(self, request, patient_card_id):
        form = PatientEntryForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("web:doctor-room-pat", patient_card_id)
        return render(request, self.template_name, {})


class PatientMakeAppoint(View):
    template_name = 'web/patient_appoint.html'

    def get(self, request):
        if not request.user.is_authenticated or request.user.position != "PT":
            return redirect('web:med-appoint')
        form = PatientMakeAppointForm()
        return render(request, self.template_name, {"form": form})

    def post(self, request):
        form = PatientMakeAppointForm(request.POST)
        if form.is_valid():
            CalendarEvent.objects.create(**form.cleaned_data, patient=request.user)
        return redirect("web:appoint-confirm")


class ClientMakeAppoint(View):
    template_name = 'web/med_appointment.html'

    def get(self, request):
        if request.user.is_authenticated and request.user.position == "PT":
            return redirect('web:patient-appoint')
        form = ClientMakeAppointForm()
        return render(request, self.template_name, {"form": form})

    def post(self, request):
        data = request.POST.dict()
        # TODO: send message to managers, to contact person
        return redirect("web:appoint-confirm")


class AppointConfirmation(View):
    template_name = 'web/appointment_confirmation.html'

    def get(self, request):
        return render(request, self.template_name, {})


class DoctorAppointments(View):
    template_name = 'web/doctor_appointments.html'

    def get(self, request):
        doctor = request.user
        appointments = doctor.schedules.all().order_by("date_time")
        return render(request, self.template_name, {"appointments": appointments})


class DoctorsListView(ListView):
    template_name = 'web/doctors_list.html'
    model = User
    context_object_name = 'doctors'

    def get_queryset(self):
        return self.model.objects.filter(Q(position='DR') & Q(doctorsdossier__isnull=False))


class DoctorsDossierDetailView(DetailView):
    template_name = 'web/doctors_dossier.html'
    model = DoctorsDossier
    context_object_name = 'profile'
    pk_url_kwarg = 'dossier_id'


class ContactView(View):
    template_name = 'web/contact.html'

    def get(self, request):
        return render(request, self.template_name, {})

    def post(self, request):
        # processing the form: send it to managers...
        return redirect("web:appoint-confirm")

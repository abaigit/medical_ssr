from datetime import timedelta
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.contrib.auth import get_user_model
from django.core.validators import FileExtensionValidator
from django.utils.text import slugify
from ckeditor.fields import RichTextField
from django_celery_beat.models import (
    CrontabSchedule, IntervalSchedule,
    PeriodicTask, PeriodicTasks,
)
from .utils import (patient_card_dir_path, patient_card_dir_path_level,
                    limit_patient_choices, limit_note_entry, file_extensions,
                    limit_doctor_choices, limit_doctors_dosier_choices)

User = get_user_model()


class Service(models.Model):
    name = models.CharField(max_length=500)
    description = models.TextField(null=True, blank=True)
    price = models.DecimalField(max_digits=10, decimal_places=2, default=250)
    cover = models.ImageField(upload_to='services')

    def __str__(self):
        return self.name


class Department(models.Model):
    name = models.CharField(max_length=500)
    slug = models.SlugField(max_length=500, blank=True)
    description = RichTextField()
    services_list = models.ManyToManyField(Service, related_name='depart_services', blank=True, default=None)
    cover = models.ImageField(upload_to='departments')

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name, allow_unicode=True)
        super(Department, self).save(*args, **kwargs)


class DoctorsDossier(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, limit_choices_to=limit_doctor_choices)
    job_position = models.CharField(max_length=255)
    specialization = models.CharField(max_length=255)
    biography = models.JSONField(null=True, blank=True)

    def __str__(self):
        return f"{self.user.email} ({self.job_position})"


class PatientCard(models.Model):  # todo: change to 'PatientFile'
    user = models.OneToOneField(User, on_delete=models.RESTRICT, limit_choices_to=limit_patient_choices)
    created = models.DateTimeField(auto_now_add=True)
    # notes_file = models.FileField(upload_to=patient_card_dir_path,
    #                               validators=[FileExtensionValidator(allowed_extensions=file_extensions())])

    def __str__(self):
        return f"UID: {self.user.pk} - {self.user.get_fullname()} - {self.user.email}"


class PatientNoteEntry(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    patient_card = models.ForeignKey(PatientCard, on_delete=models.CASCADE)
    note = RichTextField(null=True, blank=True)
    made_by = models.ForeignKey(User, on_delete=models.RESTRICT, limit_choices_to=limit_note_entry, default=1) # remove default

    class Meta:
        verbose_name_plural = "Patient Notes"
        verbose_name = 'Patient Note'

    def __str__(self):
        return f"Note for card #{self.patient_card.pk} - {self.patient_card.user.email}"


class PatientAnalysis(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    patient_card = models.ForeignKey(PatientCard, on_delete=models.CASCADE)
    csv_file = models.FileField(upload_to=patient_card_dir_path_level,
                                validators=[FileExtensionValidator(allowed_extensions=file_extensions())])

    class Meta:
        verbose_name_plural = "Patient Analyzes"
        verbose_name = 'Patient Analys'

    def __str__(self):
        return f"Test result for card #{self.patient_card.pk} - {self.patient_card.user.email}"


class CalendarEvent(models.Model):
    STATUSES = (
        ('open', 'open'),
        ('closed', 'closed'),
        ('delay', 'delay'),
        ('processing', 'processing'),
        ('canceled', 'canceled')
    )
    doctor = models.ForeignKey(User, on_delete=models.CASCADE, limit_choices_to=limit_doctor_choices, related_name="schedules")
    patient = models.ForeignKey(User, on_delete=models.CASCADE, limit_choices_to=limit_patient_choices)
    date_time = models.DateTimeField()
    notes = models.TextField(blank=True, null=True, default=None) # todo: remove defaults on production
    status = models.CharField(max_length=20, choices=STATUSES, default='open')

    def __str__(self):
        return f"Event #{self.id} ({self.date_time}) - ({self.status})"


class EventComment(models.Model):
    event = models.ForeignKey(CalendarEvent, on_delete=models.SET_NULL, null=True)
    author = models.ForeignKey(User, on_delete=models.SET_NULL, related_name="event_notes",
                               limit_choices_to=limit_note_entry(), null=True)
    comment = models.TextField()
    timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"Comment to {self.event.date_time}"


let patientCards = document.getElementsByClassName("patient-card-item");
let expandBtns = Array.from(document.getElementsByClassName("btn-expand"));

patientCards = Array.from(patientCards)

const cardMinify = (card) => {
    let cardText = card.querySelector(".card-text");
    if (cardText.scrollHeight > 170) {
        cardText.classList.add("card-minimize");
    } else {
        let btn = card.querySelector(".btn-expand");
        btn.classList.remove("btn-expand");
        btn.classList.add("disabled");
    }
}

const cardToggle = (event) => {
    let btn = event.target;
    if (!btn.classList.contains("btn-expand")) {
        event.preventDefault();
        return false
    }

    let cardText = event.target.parentElement.querySelector(".card-text");

    cardText.classList.toggle("card-minimize");
    btn.innerText = cardText.classList.contains("card-minimize") ? "Expand" : "Minimize";
}

patientCards.forEach(card => {
    cardMinify(card);
});

expandBtns.forEach(btn => {
    // $(btn).on('click', cardToggle);
    btn.addEventListener('click', cardToggle)
});

$("#getCardBtn").click(async function (e) {
    e.preventDefault();
    $("#getCardBtn").addClass("disabled");
    $("#loadSpinner").addClass("spinner-grow spinner-grow-sm");
    let cardId = $("#getCardBtn").data("card");
    let url = `/patient/card/${cardId}/download/`;

    let response = await fetch(url, {
        headers: {
            'X-CSRFToken': csrftoken,
            "Content-Type": "application/pdf; charset=UTF-8",
            'Accept': 'application/pdf',
        },
        mode: 'same-origin',
    })

    if (response.ok) {
        let blob = await response.blob();
        let dwnld_url = window.URL.createObjectURL(blob);
        $("#getCardBtn").remove();
        $(`<a href='${dwnld_url}' class='dwnldRBtn btn btn-outline-primary' download="card.pdf">Get it</a>`)
            .appendTo($("#getBtnRow"));
    } else {
        alert("HTTP ERROR", response.status);
    }
});

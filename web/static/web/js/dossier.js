let bio = JSON.parse(document.getElementById('profile-bio').textContent);

let tabs = document.getElementById("tabsBlock");
let tabContent = document.getElementById("tabContent");

for (let [year, data] of Object.entries(bio)) {
    let button = `<button class="nav-link" id="v-pills-${year}-tab" data-bs-toggle="pill" data-bs-target="#v-pills-${year}-cnt" type="button" role="tab" aria-controls="v-pills-${year}-cnt" aria-selected="false">${year}</button>`
    $(button).appendTo(tabs);

    let tabPane = $(`
    <div class="tab-pane fade" id="v-pills-${year}-cnt" role="tabpanel" aria-labelledby="v-pills-${year}-tab">
    </div>`)
    
    for (let [key, value] of Object.entries(data)) {
        let content = `<div>${key}: ${value}</div>`
        $(tabPane).append(content);
    }

    $(tabPane).appendTo(tabContent);
}

$(tabs).children(":first-child").attr('aria-selected', true);
$(tabs).children(":first-child").addClass("active");

$(tabContent).children(":first-child").addClass("active show");

$("#statusSelect").change(function (e) {
    e.preventDefault();

    let val = e.target.value;
    let cardList = Array.from($(".pt-card"));

    cardList.forEach(card => {
        if (!$(card).hasClass(val)) {
            $(card).css("display", "none");
        } else {
            $(card).css("display", "block");
        }
    })

});

$("#statusFilterReset").click(function (e) {
    e.preventDefault();

    $(".pt-card").css("display", "block");
    $("#statusSelect").val("none");
});

$(".CardStatusSelect").change((e) => {
    e.preventDefault();
    let item = e.target;
    let ap_id = $(item).data("id");
    if (item.value == "none") return;

    fetch(`/web/api/doctor/appointments/${ap_id}/`, {
        method: "PATCH",
        body: JSON.stringify({ "status": item.value }),
        headers: {
            'X-CSRFToken': csrftoken,
            "Content-Type": "application/json; charset=utf8",
            'Accept': 'application/json',
        },
        mode: 'same-origin',
    })  .then(res => res.json())
        .then(data => {
            setTimeout(() => location.reload(), 300) // таймаут, чтобы было видно обновление
        })
})
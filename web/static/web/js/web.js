// Search Patient
$(document).ready(() => {


    const renderPatientsList = (dataList) => {
        for (let i = 0; i < dataList.length; i++) {
            let elem = dataList[i];
            $('#ptFoundListId').append(`
                <div class="card mb-3">
                    <div class="card-body">
                        <h5 class="card-title">${elem.user.first_name} ${elem.user.last_name}</h5>
                        <p class="card-text">${elem.user.email}</p>
                        <p class="card-text">${elem.user.birth_date}</p>
                        <p class="card-text">${elem.user.phone_number}</p>
                        <a href="patient_card/${elem.id}/" class="btn btn-sm btn-outline-primary">Open card</a>
                    </div>
                </div>`
            )
        }
    }

    const emptyFoundList = () => {
        $('#ptFoundListId').empty();
    }

    const openPatientFile = (patientId) => {

    }


    $("#ptSearchForm").submit((e) => {
        e.preventDefault();
        let search_name = $("#doc-pt-search").val();

        fetch("/web/api/patient/search/", {
            method: 'POST',
            headers: {
                'X-CSRFToken': csrftoken,
                "Content-Type": "application/json; charset=utf8",
                'Accept': 'application/json',
            },
            mode: 'same-origin', // todo: change on prod i guess
            body: JSON.stringify({
                "search_name": search_name
            })
        })
            .then(res => res.json())
            .then(data => {
                emptyFoundList();
                renderPatientsList(data);
            });
    })



});



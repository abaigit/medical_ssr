const getEventsList = async (doc_id) => {
    let response = await fetch(`/web/api/doctor/appointments/${doc_id}/`, {
        method: 'GET',
        headers: {
            'X-CSRFToken': csrftoken,
            "Content-Type": "application/json; charset=utf8",
            'Accept': 'application/json',
        },
        mode: 'same-origin',
    });
    if (response.ok) {
        let data = await response.json();
        data.sort(function (a, b) {
            return new Date(a.date_time) - new Date(b.date_time);
        });
        data.forEach(element => {
            let busy_hour = new Date(element.date_time).getHours();

            $("#calendar").evoCalendar('addCalendarEvent', [
                {
                    id: element.id, // Event's ID (required)
                    name: `${busy_hour}:00 (Busy)`, // Event name (required)
                    date: new Date(element.date_time).toLocaleDateString(), // Event date (required)
                    type: "event", // Event type (required) // options: "birthday", "event", "holiday"
                    everyYear: false // Same event every year (optional)
                },
            ]);
        });
    } else {
        alert("HTTP Error!: " + response.status);
    }
};

const makeCalendar = (theme = 'Midnight Blue') => {
    $("#calendar").evoCalendar({
        theme: theme,
        'format': 'dd MM, yyyy',
        calendarEvents: [],
    });
    $("#calendar").find("button").attr("type", "button"); // prevent Submission
}

let newEventDate = ""; // format = "2022-04-22T07:06:29.333867Z";

makeCalendar();
getEventsList($("#id_doctor").val());


$("#id_doctor").change(function (e) {
    e.preventDefault();
    $('#calendar').evoCalendar('destroy');
    let doc_id = e.target.value;

    makeCalendar();
    getEventsList(doc_id);
});


$("#calendar").on('selectDate', function (event, newDate, oldDate) {
    let active_events = $('#calendar').evoCalendar('getActiveEvents');
    let options = [];
    let time_booked = [];
    newEventDate = '';

    $(".remove").remove();

    if (active_events.length > 0) {
        for (let i = 0; i < active_events.length; i++) {
            const element = active_events[i];
            let init_time = +element.name.split(":")[0].trim();
            let parsed_hour = element.name.includes("PM") ? init_time + 12 : init_time;
            time_booked.push(parsed_hour)
        }
    }
    for (let i = 8; i < 20; i++) {
        if (time_booked.includes(i)) continue;
        options.push(`<option value="${i}">${i}:00</option>`);
    }

    let html = `
        <label for="timeEvoSelect_Id" class="remove mt-4">Choose your time</label>
        <select name="timeEvoSelect" id="timeEvoSelect_Id" class="form-control remove bg-dark text-light">
            ${options}
        </select>
        <button type="button" id="bookBtn" class="remove text-light btn btn-outline-primary bg-info mt-3 w-100" style="background:#117a8b;">book</button>
        `
    $(html).appendTo(".calendar-events");

    $("#bookBtn").click((e) => {
        e.preventDefault();
        $(".labTim").remove();
        let selected_time = +$("#timeEvoSelect_Id").val();
        newEventDate = new Date(newDate);
        newEventDate.setHours(selected_time);
        $(`<label class="remove mt-4 labTim">
            You choose: ${newEventDate.getHours()}:00
            </label>`).appendTo(".calendar-events");
    });
});


$("#to-appoint-form").submit(function (e) {
    if (!newEventDate) {
        e.preventDefault();
        alert("Book time to visit, please");
        return false;
    }
    $("<input />").attr("type", "hidden")
        .attr("name", "date_time")
        .attr("value", newEventDate.toISOString())
        .appendTo("#to-appoint-form");
});

from rest_framework import serializers
from account.api.serializers import UserBaseSerializer
from web.models import (PatientCard, PatientAnalysis,
                        PatientNoteEntry, CalendarEvent, EventComment)


class PatientCardSerializer(serializers.ModelSerializer):
    user = UserBaseSerializer()
    entries = serializers.SerializerMethodField()
    tests = serializers.SerializerMethodField()

    class Meta:
        model = PatientCard
        fields = '__all__'
        depth = 1

    def get_entries(self, inst):
        return PatientNoteEntrySerializer(instance=inst.patientnoteentry_set.all(), many=True).data

    def get_tests(self, inst):
        return PatientTestsSerializer(instance=inst.patientanalysis_set.all(), many=True).data


class PatientNoteEntrySerializer(serializers.ModelSerializer):
    # patient_card = PatientCardSerializer()

    class Meta:
        model = PatientNoteEntry
        fields = '__all__'
        depth = 0


class PatientTestsSerializer(serializers.ModelSerializer):
    class Meta:
        model = PatientAnalysis
        fields = '__all__'
        depth = 1


class CalendarEventSerializer(serializers.ModelSerializer):
    patient = UserBaseSerializer()
    doctor = UserBaseSerializer()

    class Meta:
        model = CalendarEvent
        fields = '__all__'
        depth = 1


"""
class PatientTestsSerializer(serializers.ModelSerializer):
    class Meta:
        model = PatientTests
        fields = '__all__'
        depth = 1


class PatientFileSerializer(serializers.ModelSerializer):
    notes = serializers.SerializerMethodField()
    tests = serializers.SerializerMethodField()
    user = UserBaseSerializer()

    class Meta:
        model = PatientFile
        fields = '__all__'
        depth = 1

    def get_notes(self, obj):
        return PatientNoteEntrySerializer(instance=obj.patientnoteentry_set.all(), many=True).data

    def get_tests(self, obj):
        return PatientTestsSerializer(instance=obj.patienttests_set.all(), many=True).data
"""
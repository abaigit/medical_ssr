from django.urls import path
from . import views

app_name = 'web-api'

urlpatterns = [
    path('patient/search/', views.PatientSearchAPIView.as_view(), name='patient-search'),
    path('patient/card/<int:pk>/', views.PatientCardDetailView.as_view(), name='patient-card'),

    path('doctor/appointments/<int:doc_id>/', views.CalendarEventListAPIView.as_view(), name="doc-aps-list"),
    path('doctor/appointments/<int:id>/', views.CalendarEventRUDAPIView.as_view(), name="doc-aps-detail"),
]

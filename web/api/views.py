from django.contrib.auth import get_user_model
from django.db.models.query_utils import Q
from rest_framework.generics import ListAPIView, RetrieveAPIView, RetrieveUpdateDestroyAPIView
from rest_framework.views import APIView
from rest_framework.response import Response
from django_filters.rest_framework import DjangoFilterBackend
from account.api.serializers import UserBaseSerializer
from .serializers import (PatientCard, PatientAnalysis, PatientNoteEntry,
                          PatientCardSerializer, PatientNoteEntrySerializer,
                          CalendarEventSerializer)
from web.models import CalendarEvent

User = get_user_model()


class PatientSearchAPIView(ListAPIView):
    queryset = PatientCard.objects.all()
    serializer_class = PatientCardSerializer

    def get_queryset(self):
        search_name = self.request.data.get("search_name")
        qs = PatientCard.objects.filter(user__position="PT").filter(
            Q(user__first_name__icontains=search_name) |
            Q(user__last_name__icontains=search_name) |
            Q(user__email__icontains=search_name) |
            Q(user__phone_number__icontains=search_name)
        ).distinct()
        return qs

    def post(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class PatientCardDetailView(RetrieveAPIView):
    serializer_class = PatientCardSerializer
    queryset = PatientCard.objects.all()
    lookup_field = 'pk'


class PatientNoteEntryAPIView(RetrieveAPIView):
    serializer_class = PatientNoteEntry
    queryset = PatientNoteEntry.objects.all()


class CalendarEventListAPIView(ListAPIView):
    serializer_class = CalendarEventSerializer

    def get_queryset(self):
        doc_id = self.kwargs.get("doc_id")
        qs = CalendarEvent.objects.filter(doctor=doc_id).distinct()
        return qs

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class CalendarEventRUDAPIView(RetrieveUpdateDestroyAPIView):
    serializer_class = CalendarEventSerializer
    queryset = CalendarEvent.objects.all()
    lookup_url_kwarg = 'id'

from datetime import timedelta
from django.db.models.signals import post_save
from django.dispatch import receiver
from .models import CalendarEvent
from .tasks import send_visit_reminder


@receiver(post_save, sender=CalendarEvent)
def calendar_event_post_save(sender, instance, created, *args, **kwargs):
    if created:
        event_time = instance.date_time
        reminder_time = event_time - timedelta(days=1)
        str_time = str(reminder_time.strftime("%Y-%m-%d %H:%M"))
        send_visit_reminder.apply_async(eta=reminder_time,
                                        kwargs={"doctor": instance.doctor.get_fullname(),
                                                "patient_id": instance.patient.id,
                                                "date_time": str_time})

from calendar import HTMLCalendar
from django import forms
from django.contrib.auth import get_user_model
from .models import PatientNoteEntry, PatientCard, PatientAnalysis, Department

User = get_user_model()


class PatientEntryForm(forms.ModelForm):
    class Meta:
        model = PatientNoteEntry
        fields = "__all__"

    def __init__(self, *args, **kwargs):
        # cus_user = kwargs.pop('cus_user', None)
        super(PatientEntryForm, self).__init__(*args, **kwargs)
        self.fields['patient_card'].widget.attrs['readonly'] = True


class DateInput(forms.DateInput):
    input_type = 'date_time'


class DocModelChoiceField(forms.ModelChoiceField):
    def label_from_instance(self, obj):
        if obj._meta.model_name == "user":
            return f"{obj.get_fullname()}"
        else:
            return obj.name


class PatientMakeAppointForm(forms.Form):
    doctor = DocModelChoiceField(User.objects.filter(position="DR"), empty_label=None,
                                 widget=forms.Select(attrs={'class': 'form-control'}))
    date_time = forms.DateTimeField(widget=forms.DateTimeInput(format='%Y-%m-%d %H:%M:%S',
                                                               attrs={"class": "form-control",
                                                                      "type": 'datetime-local'}))
    notes = forms.CharField(widget=forms.Textarea(attrs={
        "class": "form-control",
        "id": "message",
        "rows": 2,
        "placeholder": "Your Message",
    }), required=False)


class ClientMakeAppointForm(PatientMakeAppointForm):
    department = DocModelChoiceField(Department.objects.all(), empty_label="--- Choose Department ---",
                                     widget=forms.Select(attrs={'class': 'form-control'}))

    def __init__(self):
        super(ClientMakeAppointForm, self).__init__()
        self.fields['doctor'].empty_label = "--- Choose Doctor ---"
